#!/usr/bin/env bash

# upstream package
UPSTREAM=https://github.com/persepolisdm/persepolis/archive/3.0.1.tar.gz
PACKAGE=persepolis
UPVERSION=3.0.1
FORMAT=gz
export LICENSE=gpl3
UPFILE=$PACKAGE-$UPVERSION.tar.$FORMAT

# change the dir to /tmp and do everything there
cd ~
rm -rf $PACKAGE-deb-dir
mkdir $PACKAGE-deb-dir
cd $PACKAGE-deb-dir

# packaging prereq
sudo apt install dh-make python3-all

# package prereq
sudo apt install aria2 sound-theme-freedesktop libnotify-bin python3-pyqt5 libqt5svg5 python3-requests python3-setproctitle python3-setuptools python3-psutil

# packager
export DEBFULLNAME='Salman Mohammadi'
export DEBEMAIL='smoha@riseup.net'

# download and extract
wget $UPSTREAM -O $UPFILE
tar --extract --$FORMAT -vf $UPFILE
rm $UPFILE
cd $PACKAGE-$UPVERSION

# create the deb package
## --indep: arch-independent binary
dh_make --yes --indep --copyright $LICENSE --createorig
cd debian
rm *.ex
rm *.EX
rm *.Debian
rm *.source
rm *.docs

rm changelog
wget https://bits.smoha.org/salman/persepolis-deb/raw/master/changelog

rm control
wget https://bits.smoha.org/salman/persepolis-deb/raw/master/control

rm copyright
wget https://bits.smoha.org/salman/persepolis-deb/raw/master/copyright

wget https://bits.smoha.org/salman/persepolis-deb/raw/master/install

cp ../man/persepolis.1 .

rm rules
wget https://bits.smoha.org/salman/persepolis-deb/raw/master/rules

echo 'man/persepolis.1.gz' > source/include-binaries

cd ..
dpkg-buildpackage -us -uc
